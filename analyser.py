import traceback
import tkinter as tk
import tkinter.ttk as ttk
import types
import sys
import os

#Source : https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
IP_PROTOCOL = {0: ('HOPOPT', 'IPv6 Hop-by-Hop Option'), 1: ('ICMP', 'Internet Control Message'), 2: ('IGMP', 'Internet Group Management'), 3: ('GGP', 'Gateway-to-Gateway'), 4: ('IPv4', 'IPv4 encapsulation'), 5: ('ST', 'Stream'), 6: ('TCP', 'Transmission Control'), 7: ('CBT', 'CBT'), 8: ('EGP', 'Exterior Gateway Protocol'), 9: ('IGP', 'any private interior gateway             \\n(used by Cisco for their IGRP)'), 10: ('BBN-RCC-MON', 'BBN RCC Monitoring'), 11: ('NVP-II', 'Network Voice Protocol'), 12: ('PUP', 'PUP'), 13: ('ARGUS (deprecated)', 'ARGUS'), 14: ('EMCON', 'EMCON'), 15: ('XNET', 'Cross Net Debugger'), 16: ('CHAOS', 'Chaos'), 17: ('UDP', 'User Datagram'), 18: ('MUX', 'Multiplexing'), 19: ('DCN-MEAS', 'DCN Measurement Subsystems'), 20: ('HMP', 'Host Monitoring'), 21: ('PRM', 'Packet Radio Measurement'), 22: ('XNS-IDP', 'XEROX NS IDP'), 23: ('TRUNK-1', 'Trunk-1'), 24: ('TRUNK-2', 'Trunk-2'), 25: ('LEAF-1', 'Leaf-1'), 26: ('LEAF-2', 'Leaf-2'), 27: ('RDP', 'Reliable Data Protocol'), 28: ('IRTP', 'Internet Reliable Transaction'), 29: ('ISO-TP4', 'ISO Transport Protocol Class 4'), 30: ('NETBLT', 'Bulk Data Transfer Protocol'), 31: ('MFE-NSP', 'MFE Network Services Protocol'), 32: ('MERIT-INP', 'MERIT Internodal Protocol'), 33: ('DCCP', 'Datagram Congestion Control Protocol'), 34: ('3PC', 'Third Party Connect Protocol'), 35: ('IDPR', 'Inter-Domain Policy Routing Protocol'), 36: ('XTP', 'XTP'), 37: ('DDP', 'Datagram Delivery Protocol'), 38: ('IDPR-CMTP', 'IDPR Control Message Transport Proto'), 39: ('TP++', 'TP++ Transport Protocol'), 40: ('IL', 'IL Transport Protocol'), 41: ('IPv6', 'IPv6 encapsulation'), 42: ('SDRP', 'Source Demand Routing Protocol'), 43: ('IPv6-Route', 'Routing Header for IPv6'), 44: ('IPv6-Frag', 'Fragment Header for IPv6'), 45: ('IDRP', 'Inter-Domain Routing Protocol'), 46: ('RSVP', 'Reservation Protocol'), 47: ('GRE', 'Generic Routing Encapsulation'), 48: ('DSR', 'Dynamic Source Routing Protocol'), 49: ('BNA', 'BNA'), 50: ('ESP', 'Encap Security Payload'), 51: ('AH', 'Authentication Header'), 52: ('I-NLSP', 'Integrated Net Layer Security  TUBA'), 53: ('SWIPE (deprecated)', 'IP with Encryption'), 54: ('NARP', 'NBMA Address Resolution Protocol'), 55: ('MOBILE', 'IP Mobility'), 56: ('TLSP', 'Transport Layer Security Protocol        \\nusing Kryptonet key management'), 57: ('SKIP', 'SKIP'), 58: ('IPv6-ICMP', 'ICMP for IPv6'), 59: ('IPv6-NoNxt', 'No Next Header for IPv6'), 60: ('IPv6-Opts', 'Destination Options for IPv6'), 62: ('CFTP', 'CFTP'), 64: ('SAT-EXPAK', 'SATNET and Backroom EXPAK'), 65: ('KRYPTOLAN', 'Kryptolan'), 66: ('RVD', 'MIT Remote Virtual Disk Protocol'), 67: ('IPPC', 'Internet Pluribus Packet Core'), 69: ('SAT-MON', 'SATNET Monitoring'), 70: ('VISA', 'VISA Protocol'), 71: ('IPCV', 'Internet Packet Core Utility'), 72: ('CPNX', 'Computer Protocol Network Executive'), 73: ('CPHB', 'Computer Protocol Heart Beat'), 74: ('WSN', 'Wang Span Network'), 75: ('PVP', 'Packet Video Protocol'), 76: ('BR-SAT-MON', 'Backroom SATNET Monitoring'), 77: ('SUN-ND', 'SUN ND PROTOCOL-Temporary'), 78: ('WB-MON', 'WIDEBAND Monitoring'), 79: ('WB-EXPAK', 'WIDEBAND EXPAK'), 80: ('ISO-IP', 'ISO Internet Protocol'), 81: ('VMTP', 'VMTP'), 82: ('SECURE-VMTP', 'SECURE-VMTP'), 83: ('VINES', 'VINES'), 84: ('IPTM', 'Internet Protocol Traffic Manager'), 85: ('NSFNET-IGP', 'NSFNET-IGP'), 86: ('DGP', 'Dissimilar Gateway Protocol'), 87: ('TCF', 'TCF'), 88: ('EIGRP', 'EIGRP'), 89: ('OSPFIGP', 'OSPFIGP'), 90: ('Sprite-RPC', 'Sprite RPC Protocol'), 91: ('LARP', 'Locus Address Resolution Protocol'), 92: ('MTP', 'Multicast Transport Protocol'), 93: ('AX.25', 'AX.25 Frames'), 94: ('IPIP', 'IP-within-IP Encapsulation Protocol'), 95: ('MICP (deprecated)', 'Mobile Internetworking Control Pro.'), 96: ('SCC-SP', 'Semaphore Communications Sec. Pro.'), 97: ('ETHERIP', 'Ethernet-within-IP Encapsulation'), 98: ('ENCAP', 'Encapsulation Header'), 100: ('GMTP', 'GMTP'), 101: ('IFMP', 'Ipsilon Flow Management Protocol'), 102: ('PNNI', 'PNNI over IP'), 103: ('PIM', 'Protocol Independent Multicast'), 104: ('ARIS', 'ARIS'), 105: ('SCPS', 'SCPS'), 106: ('QNX', 'QNX'), 107: ('A/N', 'Active Networks'), 108: ('IPComp', 'IP Payload Compression Protocol'), 109: ('SNP', 'Sitara Networks Protocol'), 110: ('Compaq-Peer', 'Compaq Peer Protocol'), 111: ('IPX-in-IP', 'IPX in IP'), 112: ('VRRP', 'Virtual Router Redundancy Protocol'), 113: ('PGM', 'PGM Reliable Transport Protocol'), 115: ('L2TP', 'Layer Two Tunneling Protocol'), 116: ('DDX', 'D-II Data Exchange (DDX)'), 117: ('IATP', 'Interactive Agent Transfer Protocol'), 118: ('STP', 'Schedule Transfer Protocol'), 119: ('SRP', 'SpectraLink Radio Protocol'), 120: ('UTI', 'UTI'), 121: ('SMP', 'Simple Message Protocol'), 122: ('SM (deprecated)', 'Simple Multicast Protocol'), 123: ('PTP', 'Performance Transparency Protocol'), 124: ('ISIS over IPv4', 'ISIS over IPv4'), 125: ('FIRE', 'FIRE'), 126: ('CRTP', 'Combat Radio Transport Protocol'), 127: ('CRUDP', 'Combat Radio User Datagram'), 128: ('SSCOPMCE', 'SSCOPMCE'), 129: ('IPLT', 'IPLT'), 130: ('SPS', 'Secure Packet Shield'), 131: ('PIPE', 'Private IP Encapsulation within IP'), 132: ('SCTP', 'Stream Control Transmission Protocol'), 133: ('FC', 'Fibre Channel'), 134: ('RSVP-E2E-IGNORE', 'RSVP-E2E-IGNORE'), 135: ('Mobility Header', 'Mobility Header'), 136: ('UDPLite', 'UDPLite'), 137: ('MPLS-in-IP', 'MPLS-in-IP'), 138: ('manet', 'MANET Protocols'), 139: ('HIP', 'Host Identity Protocol'), 140: ('Shim6', 'Shim6 Protocol'), 141: ('WESP', 'Wrapped Encapsulating Security Payload'), 142: ('ROHC', 'Robust Header Compression'), 143: ('Ethernet', 'Ethernet')}

DNS_TYPE = {
    1:'A',
    2:'NS',
    5:'CNAME',
    6:'SOA',
    11:'WKS',
    12:'PTR',
    13:'HINFO',
    14:'MINFO',
    15:'MX',
    16:'TXT',
    28:'AAAA'
}

def ip4_options(val,data,offset):
    val = int(val,16)
    dic = {
        1: ("No Operation",1,[]),
        7: ("Record Route",int(data[offset+1]),lambda:record_route(data,offset)),
        68: ("Time Stamp",int(data[offset+1]),["Not implemented"]),
        131: ("Loose Routing",int(data[offset+1]),["Not implemented"]),
        137: ("Strict Routing",int(data[offset+1]),["Not implemented"]),
    }
    try:
        return dic[val]
    except KeyError:
        return ("Unknown",0,[])
    
def record_route(data,offset):
    a = []
    res = {"Adresses :":a}
    for i in range((int(data[offset+1],16) - 3) // 4):
        offs = 3 + 4 * i
        if (offs + 1) == int(data[offset+2],16):
            break
        a.append(data[offset + offs]+"."+data[offset + offs + 1]+
            "." + data[offset + offs + 2]+"." + data[offset + offs+3])

    if a:
        return [res]
    return []





DHCPop=[1,3,6,12,15,28,31,33,43,44,46,47,50,51,53,54,55,58,59,60,61,81,119,121,249,252,255]
def option(i,data,k=0):
    un="unknown"
    if i not in DHCPop:
        return [un]
    return {1:["Subnet Mask",f"Subnet Mask: {int(data[k],16)}.{int(data[k+1],16)}.{int(data[k+2],16)}.{int(data[k+3],16)}"]
                    ,3:["Router",f"Router: {int(data[k],16)}.{int(data[k+1],16)}.{int(data[k+2],16)}.{int(data[k+3],16)}"]
                    ,6:["Domain Name Server",f"Domain Name Server: {int(data[k],16)}.{int(data[k+1],16)}.{int(data[k+2],16)}.{int(data[k+3],16)}",f"Domain Name Server: {int(data[k+4],16)}.{int(data[k+5],16)}.{int(data[k+6],16)}.{int(data[k+7],16)}"]
                    ,12:["Host Name",hname]
                    ,15:["Domain Name",un]
                    ,28:["Broadcast Address",f"Broadcast Address: {int(data[k],16)}.{int(data[k+1],16)}.{int(data[k+2],16)}.{int(data[k+3],16)}"]
                    ,31:["Perform Router Discover",un]
                    ,33:["Static Route",un]
                    ,43:["Vendor-Specific Information",vaname]
                    ,44:["NetBIOS over TCP/IP Name Server",un]
                    ,46:["NetBIOS over TCP/IP Node Type",un]
                    ,47:["NetBIOS over TCP/IP Scope",un]
                    ,50:["Requested IP Adress",f"Requested IP Adress: {int(data[k],16)}.{int(data[k+1],16)}.{int(data[k+2],16)}.{int(data[k+3],16)}"]
                    ,51:["IP Adress Lease Time",f"IP Address Lease Time: {int(data[k]+data[k+1]+data[k+2]+data[k+3],16)}s"]
                    ,53:["DHCP Message Type",f"DHCP: {['Discover','Offer','Request','*','ACK','*','*'][(int(data[k],16)-1)%7]} ({int(data[k],16)})"]  #il manque des types
                    ,54:["DHCP Server Identifier",f"DHCP Server Identifier: {int(data[k],16)}.{int(data[k+1],16)}.{int(data[k+2],16)}.{int(data[k+3],16)}"]
                    ,55:["Parameter Request List",PRL]
                    ,58:["Renewal Time Value",f"Renewal Time Value: {int(data[k]+data[k+1]+data[k+2]+data[k+3],16)}s"]
                    ,59:["Rebinding Time Value",f"Rebinding Time Value: {int(data[k]+data[k+1]+data[k+2]+data[k+3],16)}s"]
                    ,60:["Vendor class identifier",vname]
                    ,61:["Client identifier",f"Hardware type: 0x{data[k]}",f"Client MAC address: {data[k+1]}:{data[k+2]}:{data[k+3]}:{data[k+4]}:{data[k+5]}:{data[k+6]}"]
                    ,81:["Client Fully Qualified Domain Name",f"Flags: 0x{data[k]}",f"A-RR result: {int(data[k+1],16)}",f"PTR-RR result: {int(data[k+2],16)}",cname]
                    ,119:["Domain Search",un]
                    ,121:["Classless Static Route",un]
                    ,249:["Private/Classless Static Route",un]
                    ,252:["Privte/Proxy autodiscovery",un]
                    ,255:["End"]
                    }[i]

def optionN(i):
    if i not in DHCPop:
        return "unknown"
    return {1:"Subnet Mask"
                    ,3:"Router"
                    ,6:"Domain Name Server"
                    ,12:"Host Name"
                    ,15:"Domain Name"
                    ,28:"Broadcast Address"
                    ,31:"Perform Router Discover"
                    ,33:"Static Route"
                    ,43:"Vendor-Specific Information"
                    ,44:"NetBIOS over TCP/IP Name Server"
                    ,46:"NetBIOS over TCP/IP Node Type"
                    ,47:"NetBIOS over TCP/IP Scope"
                    ,50:"Requested IP Adress"
                    ,51:"IP Adress Lease Time"
                    ,53:"DHCP Message Type"
                    ,54:"DHCP Server Identifier"
                    ,55:"Parameter Request List"
                    ,58:"Renewal Time Value"
                    ,59:"Rebinding Time Value"
                    ,60:"Vendor class identifier"
                    ,61:"Client identifier"
                    ,81:"Client Fully Qualified Domain Name"
                    ,119:"Domain Search"
                    ,121:"Classless Static Route"
                    ,249:"Private/Classless Static Route"
                    ,252:"Privte/Proxy autodiscovery"
                    ,255:"End"
                    }[i]

def hname(i,data):                           #éventuellement les fusionner en utilisant id option pour les differencier
    name=""
    for j in range(int(data[i-1],16)):
        name+=chr(int(data[j+i],16))    
    return f"Host Name: {name}"

def vname(i,data):
    name=""
    for j in range(int(data[i-1],16)):
        name+=chr(int(data[j+i],16))    
    return f"Vendor class identifier: {name}"

def vaname(i,data):
    name=""
    for j in range(int(data[i-1],16)):
        name+=chr(int(data[j+i],16))    
    return f"Value: {name}"

def cname(i,data):
    name=""
    for j in range(int(data[i-1],16)-3):
        name+=chr(int(data[j+i+3],16))    
    return f"Client name: {name}"

def PRL(i,data):
        prl=[]
        for j in range(int(data[i-1],16)):
            prl+=[f"Parameter Request List Item: ({int(data[j+i],16)}) {optionN(int(data[i+j],16))}"]
        return prl

def dns_name(i,data,dns_start):
    a = int(data[i],16)
    if a & 192 == 192: #Pointeur
        val = int(data[i+1],16) + ((a-192) << 8)
        res,_ = dns_name(dns_start + val,data,dns_start)
        return res,2
    
    
    if a == 0:
        return "", 1
    
    name=[]
    for j in range(a):
        name.append(int(data[i+j+1],16))
        text = bytes(name).decode(errors="replace")
    fin_text,offset = dns_name(i + a + 1,data,dns_start)
    return text + "." + fin_text, offset + a + 1








#Lecture fichier

def lecture_fichier(file):
    try:
        fichier = open(file)
    except:
        print("erreur de lecture du fichier",file=sys.stderr)
        sys.exit(1)

    data = fichier.read().split("0000")
    fichier.close()
    data = ["0000"+x for x in data if x!='']
    data = [x.split("\n") for x in data if x!='']
    data = [[x.split() for x in y if x!=''] for y in data if y!='']
    data2=[]

    for i in range(len(data)):
        data2+=[[]]
        for j in range(len(data[i])):
            data2[-1]+=[[]]
            try:
                int(data[i][j][0],16)
                data2[-1][-1]=[data[i][j][0]]
                try:
                    for k in range(1,len(data[i][j])):
                        int(data[i][j][k],16)
                        data2[-1][j]+=[data[i][j][k]]
                except:
                    continue
            except:
                continue
    data2=[[x for x in y if x!=[]] for y in data2]

    data=[]
    ofstp=0
    err={}
    ban=[]
    for j in range(len(data2)):
        data+=[[]]
        for i in range(len(data2[j])):
            data[j]+=[[]]
            if i>=len(data2[j])-1:
                data[j][i]=data2[j][i][1:]
                ofst=0
            else:
                ofst=int(data2[j][i+1][0],16)-int(data2[j][i][0],16)+ofstp
            if ofst<=0:
                    ofstp=ofst
            else:
                ofstp=0
                if ofst<=len(data2[j][i])-1:
                    data[j][i]=data2[j][i][1:ofst+1]
                else:
                    ban+=[j]
                    err[j]=i
    ban=set(ban)
    data2=[]
    for i in data:
        data2+=[[]]
        for j in i:
            data2[-1]+=j

    return data2,ban,err

#Traitement des données
def traitement_donnes(data2,ban,err):
    EthLen = 14
    dataT={}
    i = 0

    for data in data2:
        try:
            if len(data) == 0:
                continue
            i+=1
            if i-1 in ban:
                dataT[f"trame {i}: Erreur"]=f"Erreur ligne {err[i-1]+1}, ligne incomplete"
                continue
            
            #Ethernet
            dst=f"Destination: {data[0]}"
            for j in data[1:6]:
                dst+=f":{j}"

            src=f"Source: {data[6]}"
            for j in data[7:12]:
                src+=f":{j}"
            
            typ="Type: "
            if data[12:14] == ["08","00"]:
                typ+="IPv4"
                typE="Internet Protocol Version 4"
            elif data[12:14] == ["86","dd"]:
                typ+="IPv6"
                typE="Internet Protocol Version 6"
            else:
                typ+="??"
                typE="Unknown"
                dataT[f"trame {i}"]={"Ethernet":[dst,src,typ],"Unknown":[]}
                continue
            typ+=f" (0x{data[12]+data[13]})"

            dataT[f"trame {i}"]={"Ethernet":[dst,src,typ]}

            #IPv4
            if typE=="Internet Protocol Version 4":
                HLen = int(data[EthLen][1],16)*4
                dataF0 = bin(int(data[20],16))
                dataF=""
                for x in range(8):
                    if x<len(dataF0)-2:
                        dataF+=dataF0[x+2]
                    else:
                        dataF="0"+dataF

                if int(data[23],16) == 17:
                    Proto = ["UDP","User Datagram Protocol"]
                else:
                    try:
                       Proto = IP_PROTOCOL[int(data[23],16)]
                    except KeyError:
                        Proto = ("Unknow","Unknow")

            
                dataT[f"trame {i}"][typE]=[f"Version: {data[14][0]}"
                                        ,f"Header Lengh: {HLen} bytes ({data[14][1]})"
                                        ,f"Differentiated Services Field: 0x{data[15]}"
                                        ,f"Total Length: {int(data[16]+data[17],16)}"
                                        ,f"Identification: 0x{data[18]+data[19]}"
                                        ,{f"Flags: 0x{data[20]}":
                                             [f"{dataF[0]}... .... = Reserved bit:{' not'*(1-int(dataF[0]))} set"
                                              ,f".{dataF[1]}.. .... = Don't frament:{' not'*(1-int(dataF[1]))} set"
                                              ,f"..{dataF[2]}. .... = More fragments:{' not'*(1-int(dataF[2]))} set"]}
                                        ,f"Fragment Offset: {int(data[20]+data[21],16)%8192}"
                                        ,f"Time to Live: {int(data[22],16)}"
                                        ,f"Protocol: {Proto[0]} ({int(data[23],16)})"
                                        ,f"Header Checksum: 0x{data[24]+data[25]}"
                                        ,f"Source Address: {int(data[26],16)}.{int(data[27],16)}.{int(data[28],16)}.{int(data[29],16)}"
                                        ,f"Destination Address: {int(data[30],16)}.{int(data[31],16)}.{int(data[32],16)}.{int(data[33],16)}"]
                #Options Ipv4
                taille = 20
                while taille < HLen:
                    opt_val = data[EthLen + taille]
                    if opt_val == "00":
                        break
                    opt,length, infos = ip4_options(opt_val,data, EthLen+taille)
                   
                    if length == 0:
                        dataT[f"trame {i}"][typE].append("Unknow option")
                        break

                    text_opt = f"Option: ({opt_val}) {opt}"
                    text_length = f"Length: {length}"
                    dataT[f"trame {i}"][typE].append({text_opt:[text_length]})

                    if isinstance(infos, types.FunctionType):
                        infos = infos()

                    dataT[f"trame {i}"][typE][-1][text_opt] += infos

                    taille += length


                
            #IPv6
            if typE=="Internet Protocol Version 6":
                HLen = 40
                
                if int(data[20],16) == 17:
                    Proto = ["UDP","User Datagram Protocol"]
                else:
                    try:
                        Proto = IP_PROTOCOL[int(data[23],16)]
                    except KeyError:
                        Proto = ("Unknow","Unknow")

                sa=""
                da=""
                for j in range(8):
                    sa+=str(hex(int(data[22+j*2]+data[23+j*2],16)))[2:]+":"
                    da+=str(hex(int(data[38+j*2]+data[39+j*2],16)))[2:]+":"
                sal=sa.split(":")[:-1]
                dal=da.split(":")[:-1]
                sa=sal[0]+":"
                for j in sal[1:]:
                    if j!="0":
                        sa+=":"+j
                da=dal[0]+":"
                for j in dal[1:]:
                    if j!="0":
                        da+=":"+j
                
                dataT[f"trame {i}"][typE]=[f"Version: {data[14][0]}"
                                        ,f"Traffic Class: 0x{data[14][1]+data[15][0]}"
                                        ,f"Flow Label: 0x{data[15][1]+data[16]+data[17]}"
                                        ,f"Payload Length: {int(data[18]+data[19],16)}"
                                        ,f"Next Header: {Proto[0]} ({int(data[20],16)})"
                                        ,f"Hop Limit: {int(data[21],16)}"
                                        ,f"Source Address: {sa}"
                                        ,f"Destination Address: {da}"]

            #UDP
            if Proto[0]=="UDP":
                sh=EthLen+HLen
                dns_start = sh + 8
                UDPLen = int(data[sh+4]+data[sh+5],16)
                srcPort = int(data[sh]+data[sh+1],16)
                dstPort = int(data[sh+2]+data[sh+3],16)
            
                dataT[f"trame {i}"][Proto[1]]=[f"Source Port: {srcPort}"
                                           ,f"Destination Port: {dstPort}"
                                           ,f"Length: {UDPLen}"
                                           ,f"Checksum: 0x{data[sh+6]+data[sh+7]}"
                                           ,f"UDP payload ({UDPLen-8} bytes)"]
            else:
                dataT[f"trame {i}"][Proto[1]]=["Protocole non géré"]
                continue
            
            #DNS
            if srcPort == 53 or dstPort == 53:
                Proto = "Domain Name System"
                    
                dataT[f"trame {i}"][Proto]=[f"Transaction ID: 0x{data[sh+8]+data[sh+9]}"
                                        ,f"Flags: 0x{data[sh+10]+data[sh+11]}"
                                        ,f"Questions: {int(data[sh+12]+data[sh+13],16)}"
                                        ,f"Answer RRs: {int(data[sh+14]+data[sh+15],16)}"
                                        ,f"Authority RRs: {int(data[sh+16]+data[sh+17],16)}"
                                        ,f"Additional RRs: {int(data[sh+18]+data[sh+19],16)}"]
                nbq = int(data[sh+12]+data[sh+13],16)
                nba=int(data[sh+14]+data[sh+15],16)
                nb_aut = int(data[sh+16]+data[sh+17],16)
                nb_add = int(data[sh+18]+data[sh+19],16)
                sh+=20
                if nbq>0:   #multiple question?
                    dataT[f"trame {i}"][Proto] += [{"Queries":[]}]
                    for _ in range(nbq):
                        name1,d=dns_name(sh,data,dns_start)
                        name1=name1[:-1]
                        sh += d
                        typ = int(data[sh]+data[sh+1],16)
                        dataT[f"trame {i}"][Proto][-1]["Queries"]+=[{f"{name1}:":
                                                  [f"Name: {name1}"
                                                   ,f"Type: {typ} ({DNS_TYPE.get(typ,'Unknow')})"
                                                   ,f"Class: 0x{data[sh+2]+data[sh+3]}"]}]
                        sh += 4


                for (var, name) in ((nba,"Answers"),(nb_aut,"Authoritative nameservers"),(nb_add,"Additional records")):

                    if var:
                        dataT[f"trame {i}"][Proto] += [{name:[]}]
                        for _ in range(var):
                            name2,d=dns_name(sh,data,dns_start)
                            name2=name2[:-1]
                            sh += d
                            typ=int(data[sh]+data[sh+1],16)
                            ttl=int(data[sh+4]+data[sh+5]+data[sh+6]+data[sh+7],16)
                            data_length = int(data[sh+8] + data[sh+9],16)
                            lst = [f"Name: {name2}"
                                   ,f"Type: {typ} ({DNS_TYPE.get(typ,'Unknow')})"
                                   ,f"Class: 0x{data[sh+2]+data[sh+3]}"
                                   ,f"Time to live: {ttl} ({ttl//3600} hours, {ttl//60%60} minutes, {ttl%60} seconds)"
                                   ,f"Data length: {int(data[sh+8] + data[sh+9],16)}"]



                            if typ == 5:
                                cname, d = dns_name(sh + 10,data,dns_start)
                                cname=cname[:-1]
                                lst.append(f"CNAM: {cname}")
                            if typ==1:
                                lst.append(f"Address: {int(data[sh+10],16)}.{int(data[sh+11],16)}.{int(data[sh+12],16)}.{int(data[sh+13],16)}")

                            sh += 10
                            sh += data_length

                            dataT[f"trame {i}"][Proto][-1][name] += [{f"{name2}:":lst}]

            #DHCP
            elif srcPort == 67 or dstPort == 67:
                Proto = "Dynamic Host Configuration Protocol"
                hostName = "not given"
                if any(int(x,16)for x in data[sh+52:sh+116]):
                    hostName=""
                    j=0
                    while int(data[sh+52+j],16)!=0:
                        char=chr(int(data[sh+21+j],16))
                        hostName+=[char]
                        j+=1
                bootName = "not given"
                if any(int(x,16)for x in data[sh+116:sh+244]):
                    bootName=""
                    j=0
                    while int(data[sh+116+j],16)!=0:
                        char=chr(int(data[sh+21+j],16))
                        hostName+=[char]
                        j+=1
                if [data[sh+244],data[sh+245],data[sh+246],data[sh+247]]==["63","82","53","63"]:
                    mc = "DHCP"
                else:
                    mc = "Unknown"
                    
                dataT[f"trame {i}"][Proto]=[f"Message type: {int(data[sh+8],16)}"
                                        ,f"Hardware type: 0x{data[sh+9]}"
                                        ,f"Hardware address length: {int(data[sh+10],16)}"
                                        ,f"Hops: {int(data[sh+11],16)}"
                                        ,f"Transaction ID: 0x{data[sh+12]+data[sh+13]+data[sh+14]+data[sh+15]}"
                                        ,f"Seconds elapsed: {int(data[sh+16]+data[sh+17],16)}"
                                        ,{f"Bootp flags: 0x{data[sh+18]+data[sh+19]}":
                                              [f"{int(data[sh+18],16)//128}... .... .... .... = Broadcast flag: { 'Unicast'*(not int(data[sh+18],16)//128)+'Broadcast'*(int(data[sh+18],16)//128) }"
                                               ,f".000 0000 0000 0000 = Reserved flags: 0x0000"]}
                                        ,f"Client IP address: {int(data[sh+20],16)}.{int(data[sh+21],16)}.{int(data[sh+22],16)}.{int(data[sh+23],16)}"
                                        ,f"Your (client) IP address: {int(data[sh+24],16)}.{int(data[sh+25],16)}.{int(data[sh+26],16)}.{int(data[sh+27],16)}"
                                        ,f"Next server IP address: {int(data[sh+28],16)}.{int(data[sh+29],16)}.{int(data[sh+30],16)}.{int(data[sh+31],16)}"
                                        ,f"Relay agent IP address: {int(data[sh+32],16)}.{int(data[sh+33],16)}.{int(data[sh+34],16)}.{int(data[sh+35],16)}"
                                        ,f"Client MAC address: {data[sh+36]}:{data[sh+37]}:{data[sh+38]}:{data[sh+39]}:{data[sh+40]}:{data[sh+41]}"
                                        ,f"Client hardware address padding: {data[sh+42]+data[sh+43]+data[sh+44]+data[sh+45]+data[sh+46]+data[sh+47]+data[sh+48]+data[sh+49]+data[sh+50]+data[sh+51]}"
                                        ,f"Server host name {hostName}"
                                        ,f"Boot file name {bootName}"
                                        ,f"Magic cookie: {mc}"]
                #DHCP Option
                j=0
                sh+=248
                while int(data[sh+j],16)!=255:
                    op=option(int(data[sh+j],16),data)
                    tOp=f"Option: ({int(data[sh+j],16)}) "+op[0]
                    dataT[f"trame {i}"][Proto]+=[{tOp:[f"Length: {int(data[sh+j+1],16)}"]}]
                    for k in range(len(op)-1):
                        opt=option(int(data[sh+j],16),data,sh+j+2)[k+1]

                        if isinstance(opt,types.FunctionType):
                            opt=opt(sh+j+2,data)
                        dataT[f"trame {i}"][Proto][-1][tOp]+=[opt]
                    j+=int(data[sh+j+1],16)+2
                dataT[f"trame {i}"][Proto]+=[f"Option: ({int(data[sh+j],16)}) "+option(255,data)[0]]
                
            #DHCPv6
            elif srcPort == 547 or dstPort == 547:
                Proto = "DHCPv6"

                dataT[f"trame {i}"][Proto]=["Protocole non géré"]
            
            else:
                Proto = "Unknown"
        except Exception:
            print(f"Error on frame {i} : ",file=sys.stderr)
            traceback.print_exc()
                        
    return dataT

#Affichage



def insertion(tree,dictio,idd=''):
    if type(dictio) is dict:
        for i in dictio:
            iid=tree.insert(idd,'end',text=i)
            insertion(tree,dictio[i],iid)
    elif type(dictio) is list:
        for i in dictio:
            insertion(tree,i,idd)
    else:
        tree.insert(idd,'end',text=dictio)

def insertfichier(f,dictio,idd=0):
    if type(dictio) is dict:
        for i in dictio:
            f.write(f"{'    '*idd}{i}\n")
            insertfichier(f,dictio[i],idd+1)
    elif type(dictio) is list:
        for i in dictio:
            insertfichier(f,i,idd)
    else:
        f.write(f"{'    '*idd}{dictio}\n")


def main():
    
    if len(sys.argv) == 2:
        data,ban,err = lecture_fichier(sys.argv[1])
    else:
        data,ban,err = lecture_fichier("input.txt")
    dataT = traitement_donnes(data,ban,err)

    try:
        fichier=open("res.txt",'w')
        insertfichier(fichier,dataT)
        fichier.close()
    except:
        print("essayer d'executer l'application sur un autre disque.")
    
    fen = tk.Tk()

    tree = ttk.Treeview(fen,heigh=25)
    tree.column("#0", width=1000, minwidth=1000, stretch=tk.NO)

    tree.pack()       
    insertion(tree,dataT)

    fen.mainloop()

if __name__ == '__main__':
    main()
