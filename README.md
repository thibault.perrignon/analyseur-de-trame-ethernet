# Projet réseau L3

Dans `input.txt`, mettre les trames ethernet.

### Exemple : 

    0000   2e 0e 3d a0 f5 9d 34 2e b7 c6 55 57 08 00 45 00
    0010   00 48 4f 98 00 00 80 11 00 00 c0 a8 2b 74 c0 a8
    0020   2b 01 d8 83 00 35 00 34 d8 0b 73 61 01 00 00 01
    0030   00 00 00 00 00 00 05 66 65 32 63 72 06 75 70 64
    0040   61 74 65 09 6d 69 63 72 6f 73 6f 66 74 03 63 6f
    0050   6d 00 00 01 00 01

Puis, exécuter `analyser.py`, une fenêtre s'ouvre et affiche
les trames sous la forme d'une liste déroulante et une version textuelle est sauvegardée dans le fichier `res.txt`