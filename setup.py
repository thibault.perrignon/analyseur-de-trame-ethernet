import sys
from cx_Freeze import setup, Executable

def executables1(i):
    return [
        Executable(script = "analyser.py",icon = "index.ico", base = ["Win32GUI",None][i%2] )
]

i=0
if len(sys.argv)>2:
    i=int(sys.argv[2])
    sys.argv=sys.argv[:2]
    
buildOptions = dict( 
        includes = ["tkinter","tkinter.ttk"],
        include_files = ["input.txt"]
)

setup(
    options = dict(build_exe = buildOptions),
    executables = executables1(i)
)
