import sys
from cx_Freeze import setup, Executable

includes = []

# Include your files and folders
includefiles = ['input.txt']

# Exclude unnecessary packages
excludes = ['cx_Freeze','pydoc_data','setuptools','distutils','asyncio','email','http','logging','unittest','urllib','xml']

# Dependencies are automatically detected, but some modules need help.
packages = []    

base = None
shortcutName = None
shortcutDir = None
if sys.platform == "win32":
    base = "Win32GUI"
    shortcutName='My App'
    shortcutDir="DesktopFolder"

setup(
    name = 'analyser',
    version = '1',
    description = '',
    author = '',
    author_email = '',
    options = {'build_exe': {
        'includes': includes,
        'excludes': excludes,
        'packages': packages,
        'include_files': includefiles}
        }, 
    executables = [Executable('analyser.py', 
    base = base, # "Console", base, # None
    icon='index.ico', 
    shortcutName = shortcutName, 
    shortcutDir = shortcutDir)]
)
